import Vue from 'vue';

import './styles/quasar.sass';
import lang from 'quasar/lang/zh-hant.js';
import '@quasar/extras/roboto-font/roboto-font.css';
import '@quasar/extras/material-icons/material-icons.css';
import { Quasar } from 'quasar';

Vue.use(Quasar, {
  config: {},
  components: {
  },
  directives: {
  },
  plugins: {
  },
  lang,
});
