import Vue from 'vue';
import App from './app.vue';
import store from './store/store';
import './quasar';
import i18n from './i18n';

Vue.config.productionTip = false;

new Vue({
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
