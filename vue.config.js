module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false,
    },
    i18n: {
      locale: 'tw',
      fallbackLocale: 'tw',
      localeDir: 'locales',
      enableInSFC: false,
    },
    electronBuilder: {
      preload: 'src/preload.js',
      nodeIntegration: true,
    },
  },
  transpileDependencies: [
    'quasar',
  ],
};
